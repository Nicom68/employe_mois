DROP DATABASE IF EXISTS employe_mois;
CREATE DATABASE employe_mois;
USE employe_mois;
 
CREATE TABLE employee (
  employee_id integer PRIMARY KEY,
  employee_name varchar(255),
  department varchar(255)
);

CREATE TABLE location (
  id_location integer PRIMARY KEY,
  Usine Enum ('Usine A', "Usine B", "Usine C")
);

CREATE TABLE working_day (
  id_working_day integer PRIMARY KEY,
  day date,
  shift_start_time time,
  shift_end_time time,
  break_duration time,
  overtime_hours time,
  id_location integer REFERENCES location(id_location),
  employee_id integer REFERENCES employee(employee_id)
);
